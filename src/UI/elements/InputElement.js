import React, { useEffect, useRef } from 'react';
import {
  Image,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  Animated,
} from 'react-native';

const WIDTH = Dimensions.get('window').width;

const emailIcon = require('../../../assets/icons/email.png');
const passwordIcon = require('../../../assets/icons/password.png');

const styles = {
  $activeShadow: {
    shadowColor: '$black',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 15,
    elevation: 7,
    zIndex: 999,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: WIDTH - 100,
    backgroundColor: '#fff',
  },
  icon: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    paddingLeft: 25,
    paddingRight: 25,
  },
  placeholder: {
    opacity: 0.4,
    fontSize: 11,
    textTransform: 'uppercase',
    width: '25%',
  },
  input: {
    flex: 1,
    fontSize: 12,
    width: '60%',
  },
};

const renderIcon = (type) => {
  switch (type) {
    case 'EMAIL':
      return emailIcon;
    case 'PASSWORD':
      return passwordIcon;
    default:
      return;
  }
};

const ANIMATION_DURATION = 300;

const InputElement = ({ options }) => {
  // USING A REFERENCE TO THE TEXTINPUT
  // IN ORDER TO MANUALLY SET FOCUS AT ONPRESS ACTION
  const inputRef = useRef();
  const inputScale = useRef(new Animated.Value(1)).current;

  const setActive = () => {
    inputRef.current.focus();

    Animated.timing(inputScale, {
      toValue: 1.15,
      duration: ANIMATION_DURATION,
      useNativeDriver: true,
    }).start();
  };

  const setInactive = () => {
    Animated.timing(inputScale, {
      toValue: 1,
      duration: ANIMATION_DURATION,
      useNativeDriver: true,
    }).start();
  };

  // MAKE SURE TO PASS OPTIONS TO SECOND PARAMETER ARRAY
  // BECAUSE COMPONENT NEEDS TO UPDATE WHEN ACTIVE STATE
  // OF COMPONENT CHANGES
  useEffect(() => {
    if (options.active) {
      setActive();
    } else {
      setInactive();
    }
  }, [options]);

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => options.onPress(options.type)}
    >
      <Animated.View
        style={[
          styles.container,
          { transform: [{ scale: inputScale }] },
          options.active && styles.$activeShadow,
        ]}
        pointerEvents="none"
      >
        <Image style={styles.icon} source={renderIcon(options.type)} />
        <Text style={styles.placeholder}>{options.placeholder}</Text>
        <TextInput
          style={styles.input}
          ref={inputRef}
          keyboardType={options.keyboardType}
          secureTextEntry={options.isPassword}
          onChangeText={options.onChangeText}
          value={options.value}
          autoCapitalize="none"
        />
      </Animated.View>
    </TouchableOpacity>
  );
};

export default InputElement;
