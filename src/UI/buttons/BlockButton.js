import React from 'react';
import { View, TouchableOpacity, Text, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { Colors } from '../../config/constants';

const WIDTH = Dimensions.get('window').width;

const styles = {
  $small: {
    width: (WIDTH - 100) / 2 - 12,
    height: 35,
  },
  $smallFont: { fontSize: 12 },

  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: WIDTH - 100,
    marginBottom: 12,
  },
  gradientView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  titleText: {
    fontSize: 14,
    letterSpacing: 5,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
};

const BlockButton = ({ title, onPress, colorOptions, textColor, small }) => {
  const color1 =
    (colorOptions && colorOptions.colorFrom) || Colors.gradientFrom;
  const color2 = (colorOptions && colorOptions.colorTo) || Colors.gradientTo;

  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
      <View style={[styles.container, small && styles.$small]}>
        <LinearGradient
          style={styles.gradientView}
          colors={[color1, color2]}
          start={[0, 0]}
          end={[1, 1]}
        >
          <Text
            style={[
              styles.titleText,
              textColor && { color: textColor },
              small && styles.$smallFont,
            ]}
          >
            {title}
          </Text>
        </LinearGradient>
      </View>
    </TouchableOpacity>
  );
};

export default BlockButton;
