import React from 'react';
import { View } from 'react-native';
import InputElement from '../elements/InputElement';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
};

// THE SIMPLEST WAY TO RENDER MULTIPLE INPUT ELEMENTS INSIDE ONE VIEW
const AuthInputView = ({ inputOptions }) => (
  <View style={styles.container}>
    {inputOptions.map((item, index) => (
      <InputElement key={index.toString()} options={item} />
    ))}
  </View>
);

export default AuthInputView;
