import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: -50,
    right: -50,
    zIndex: 99999,
    backgroundColor: '#00000099',
  },
};

const LoadingView = ({ hideSpinner }) => {
  return (
    <View style={styles.container}>
      {!hideSpinner && <ActivityIndicator size="large" color="white" />}
    </View>
  );
};

export default LoadingView;
