import React, { useEffect, useRef } from 'react';
import {
  Image,
  Text,
  AppState,
  Keyboard,
  Platform,
  Animated,
} from 'react-native';

const logoImage = require('../../../assets/images/logo.png');

const ANIMATION_DURATION = 300;

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 100,
    resizeMode: 'contain',
    margin: 50,
  },
  text: {
    fontSize: 30,
    textTransform: 'uppercase',
    letterSpacing: 15,
  },
};

const LogoView = ({ title }) => {
  // ANIMATIONS SHOULD BE STORED AND MODIFIED BY MEANS OF A REFERENCE
  // DIRECTLY MODIFYING ANIMATIONS RESULTS IN BAD PERFORMANCE
  const logoViewScale = useRef(new Animated.Value(1)).current;

  const name = Platform.OS === 'ios' ? 'Will' : 'Did';

  // DON'T FORGET TO SET NATIVEDRIVER
  const showKeyboard = () => {
    Animated.timing(logoViewScale, {
      toValue: 0.65,
      duration: ANIMATION_DURATION,
      useNativeDriver: true,
    }).start();
  };

  const hideKeyboard = () => {
    Animated.timing(logoViewScale, {
      toValue: 1,
      duration: ANIMATION_DURATION,
      useNativeDriver: true,
    }).start();
  };

  // LISTENERS TO KEEP TRACK OF THE KEYBOARD SHOW/HIDE ACTIONS
  useEffect(() => {
    AppState.addEventListener = Keyboard.addListener(
      `keyboard${name}Show`,
      showKeyboard
    );

    AppState.addEventListener = Keyboard.addListener(
      `keyboard${name}Hide`,
      hideKeyboard
    );

    // NEVER FORGET TO REMOVE YOUR LISTENERS ON
    // LEAVING THE SCREEN
    return () => {
      AppState.removeEventListener = Keyboard.removeListener(
        `keyboard${name}Show`,
        showKeyboard
      );
      AppState.removeEventListener = Keyboard.removeListener(
        `keyboard${name}Hide`,
        hideKeyboard
      );
    };
  });

  return (
    <Animated.View
      style={[styles.container, { transform: [{ scale: logoViewScale }] }]}
    >
      <Image style={styles.image} source={logoImage} />
      <Text style={styles.text}>{title}</Text>
    </Animated.View>
  );
};

export default LogoView;
