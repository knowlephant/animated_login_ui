import React from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';

import LoadingView from './LoadingView';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const ContainerView = ({ children, onKeyboardAvoidPress, loadingOptions }) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={{ flex: 1 }}
    >
      <TouchableWithoutFeedback
        onPress={onKeyboardAvoidPress}
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
          {loadingOptions && loadingOptions.loading && (
            <LoadingView hideSpinner={loadingOptions.hideSpinner} />
          )}
          {children}
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default ContainerView;
