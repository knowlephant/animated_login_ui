import React, { useContext, useState, useEffect } from 'react';
import { Text } from 'react-native';

import ContainerView from '../UI/views/ContainerView';
import BlockButton from '../UI/buttons/BlockButton';

import { Context } from '../config/store';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgray',
  },
  text: {
    fontSize: 16,
    margin: 100,
  },
};

const Home = () => {
  const { dispatch } = useContext(Context);

  const [loading, setLoading] = useState(false);

  const handleLogoutPress = async () => {
    setLoading(true);
  };

  useEffect(() => {
    let timer;

    if (loading) {
      timer = setTimeout(() => {
        dispatch({ type: 'LOGOUT' });
        setLoading(false);
      }, 1000);

      return () => clearTimeout(timer);
    }
  }, [loading]);

  return (
    <ContainerView loadingOptions={{ loading }}>
      <Text style={styles.text}>HOME</Text>
      <BlockButton title="Logout" onPress={handleLogoutPress} />
    </ContainerView>
  );
};

export default Home;
