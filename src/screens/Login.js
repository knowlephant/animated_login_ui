import React, { useEffect, useState, useContext } from 'react';
import { View, ImageBackground, Keyboard } from 'react-native';

import ContainerView from '../UI/views/ContainerView';
import LogoView from '../UI/views/LogoView';
import AuthInputView from '../UI/views/AuthInputView';
import BlockButton from '../UI/buttons/BlockButton';

import { Context } from '../config/store';

const backgroundImage = require('../../assets/images/background.png');

// FOR THIS TUTORIAL'S CONVENIENCE, I HAVE PLACED THE
// STYLESHEET INSIDE THE COMPONENT FILE
// THIS IS NOT BEST PRACTICE
const styles = {
  container: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoView: {
    flex: 3,
  },
  inputView: {
    flex: 2,
  },
  buttonView: {
    flex: 1,
    marginBottom: 50,
  },
  horizontalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

const Login = () => {
  const { dispatch } = useContext(Context);

  // LOCAL STATE TO KEEP TRACK OF INPUT VALUES
  // AND TO CONTROL THE INPUT ELEMENT ANIMATIONS
  const [email, setEamil] = useState('Test@email.com');
  const [emailActive, setEmailActive] = useState(false);
  const [password, setPassword] = useState('1234');
  const [passwordActive, setPasswordActive] = useState(false);
  const [loading, setLoading] = useState(false);
  const [hideSpinner, setHideSpinner] = useState(false);

  // OPTIONS, USED TO RENDER EACH OF THE INPUT FIELD ELEMENTS
  const inputOptions = [
    {
      type: 'EMAIL',
      keyboardType: 'email-address',
      placeholder: 'Email',
      value: email,
      onChangeText: (text) => handleTextChange(text, 'EMAIL'),
      onPress: (type) => handleInputPress(type),
      active: emailActive,
    },
    {
      type: 'PASSWORD',
      placeholder: 'Password',
      isPassword: true,
      value: password,
      onChangeText: (text) => handleTextChange(text, 'PASSWORD'),
      onPress: (type) => handleInputPress(type),
      active: passwordActive,
    },
  ];

  // THE INPUT ELEMENT ONPRESS HANDLER
  const handleInputPress = (type) => {
    switch (type) {
      case 'EMAIL':
        setEmailActive(true);
        setPasswordActive(false);
        break;
      case 'PASSWORD':
        setPasswordActive(true);
        setEmailActive(false);
        break;
      default:
        break;
    }
  };

  // THE TEXTINPUT CHANGE HANDLER
  const handleTextChange = (text, type) => {
    switch (type) {
      case 'EMAIL':
        setEamil(text);
        break;
      case 'PASSWORD':
        setPassword(text);
        break;
      default:
        break;
    }
  };

  // THE KEYBOARD DISMISS HANDLER
  // USED WHEN USER PRESSES OUTSIDE THE KEYBOARD
  const handleKeyboardDismiss = () => {
    Keyboard.dismiss();
    setEmailActive(false);
    setPasswordActive(false);
  };

  const handleStartPress = () => {
    setLoading(true);
  };

  useEffect(() => {
    let timer;

    if (loading) {
      timer = setTimeout(() => {
        dispatch({ type: 'LOGIN', data: { email, password } });
        setLoading(false);
      }, 1000);
    }

    return () => clearTimeout(timer);
  }, [loading]);

  // SIZE AND POSITION OF EACH VIEW IS CONTROLLED
  // BY SETTING FLEX STYLE PROPERTY
  return (
    <ImageBackground source={backgroundImage} style={styles.container}>
      <ContainerView
        onKeyboardAvoidPress={handleKeyboardDismiss}
        loadingOptions={{ loading, hideSpinner }}
      >
        <View style={styles.logoView}>
          <LogoView title="Sign in" />
        </View>
        <View style={styles.inputView}>
          <AuthInputView
            inputOptions={inputOptions}
            onChangeText={handleTextChange}
          />
        </View>
        <View style={styles.buttonView}>
          <BlockButton title="Start" onPress={handleStartPress} />
        </View>
      </ContainerView>
    </ImageBackground>
  );
};

export default Login;
