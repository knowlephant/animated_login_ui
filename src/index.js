import React from 'react';
import NavigationContainerStack from './config/routes';

// BE CAREFUL THAT WE EXPORT STORE AS AN OBJECT WITH A 'STORE' KEY,
// WHICH MEANS WE MUST IMPORT IT IN THE SAME WAY
import { Store } from './config/store';

export default function App() {
  return (
    <Store>
      <NavigationContainerStack />
    </Store>
  );
}
