import React, { createContext, useReducer } from 'react';
import reducer from './reducer';

// INITIALISE ALL TYPES OF STATE YOUR APP
// INITIAL STATE IS OFFERED TO CONTEXT AS A MEANS TO KEEP TRACK OF
const initialState = {
  authToken: null,
};

export const Context = createContext(initialState);

// OUR STORE CONTAINS TWO VALUES
// STATE, WHICH HOLDS ALL SAVED DATA
// DISPATCH, WHICH OFFERS A METHOD TO UPDATE OUR STATE
export const Store = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <Context.Provider value={{ state, dispatch }}>{children}</Context.Provider>
  );
};

export default { Context, Store };
