import * as SecureStore from 'expo-secure-store';

const DUMMY_EMAIL = 'Test@email.com';
const DUMMY_PASSWORD = '1234';
const DUMMY_TOKEN = 'abc123';

const setToken = async (action) => {
  await SecureStore.setItemAsync('RN_LOGIN_TOKEN', DUMMY_TOKEN);
};

const deleteTokens = async () => {
  await SecureStore.deleteItemAsync('RN_LOGIN_TOKEN');
};

// THE REDUCER HANDLES ALL INCOMING DISPATCH REQUESTS
// IT ACCEPTS THE DISPATCH AND STORES THE DISPATCH IN OUR STORE
const Reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      console.log('action', action);
      if (action.data.email && action.data.password) {
        setToken(action);

        return {
          ...state,
          authToken: DUMMY_TOKEN,
        };
      }

      return state;
    case 'SET_TOKEN':
      return {
        ...state,
        authToken: action.token,
      };
    case 'LOGOUT':
      deleteTokens();
      return {
        ...state,
        authToken: null,
      };
    default:
      return state;
  }
};

export default Reducer;
