import React, { useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as SecureStore from 'expo-secure-store';

import { Context } from './store';

import Login from '../screens/Login';
import Home from '../screens/Home';

// EACH TIME A NAVIGATION OCCURS A THE NEW SCREEN WILL BE PUSHED
// ON TOP OF A STACKNAVIGATOR. EACH BACK ACTION REMOVES THE SCREEN
// FROM THE STACKNAVIGATOR
const RootNavigationStack = createStackNavigator();

const RootStackScreen = () => {
  const { state, dispatch } = useContext(Context);

  const checkForToken = async () => {
    const emailToken = await SecureStore.getItemAsync('RN_LOGIN_TOKEN');

    if (emailToken) {
      dispatch({ type: 'SET_TOKEN', token: emailToken });
    }
  };

  useEffect(() => {
    checkForToken();
  }, []);

  return (
    <RootNavigationStack.Navigator
      screenOptions={{
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitle: () => null,
      }}
    >
      {!state.authToken ? (
        <RootNavigationStack.Screen name="Login" component={Login} />
      ) : (
        <RootNavigationStack.Screen name="Home" component={Home} />
      )}
    </RootNavigationStack.Navigator>
  );
};

// A NAVIGATIONCONTAINER CAN ONLY CONTAIN ONE ROOT NAVIGATIONSTACK
// IF YOUR APP HAS MULTIPLE NAVIGATION STACKS (SUCH AS WOULD BE THE CASE
// WITH A TAB BAR), THOSE NAVIGATIONSTACKS WOULD BE GROUPED AS SCREENS INSIDE
// YOUR ROOT NAVIGATIONSTACK
const NavigationContainerStack = () => (
  <NavigationContainer>
    <RootStackScreen />
  </NavigationContainer>
);

export default NavigationContainerStack;
