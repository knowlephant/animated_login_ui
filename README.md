# Animated Login UI

Animated Login UI is a starter kit to help you create beautiful animated login screens.

For further documentation, see the following links:

[How to build animated login screens with react native](https://medium.com/javascript-in-plain-english/how-to-build-animated-login-screens-with-react-native-edb81cb500aa)


[Creating a smooth signin and logout experience with react native](https://medium.com/javascript-in-plain-english/creating-a-smooth-sign-in-and-logout-experience-with-react-native-62e5deffbff)

## Installation

This resource is managed with Expo:

```bash
cd [your-project-name]

yarn install

yarn start
```

## License
No restrictions apply